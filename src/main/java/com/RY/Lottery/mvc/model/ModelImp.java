package com.RY.Lottery.mvc.model;

import com.RY.Lottery.Classes.BallColor;
import com.RY.Lottery.Classes.Lottery;
import com.RY.Lottery.Classes.LotteryTicket;
import com.RY.Lottery.Classes.LuckyBall;
import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import com.RY.Lottery.Exception.InvalidNumberValueOfLuckyBallException;
import com.RY.Lottery.Exception.InvalidTicketFormatException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ModelImp implements Model {

    List<LuckyBall> luckyBalls;
    Lottery lottery = new Lottery();

    @Override
    public int startLottery() {
        return lottery.start();
    }

    @Override
    public void newLottery() {
        lottery = new Lottery();
    }

    @Override
    public void createNewTicket() {
        luckyBalls = new LinkedList<>();
    }

    @Override
    public void addLuckyNumber(String color, int number) throws InvalidNumberValueOfBallColorException, InvalidNumberValueOfLuckyBallException {
        if (luckyBalls == null){
            return;
        }
        luckyBalls.add(new LuckyBall(number, BallColor.getInstance(color)));
    }

    @Override
    public int addTicket() throws FileNotFoundException, InvalidTicketFormatException {
        if (luckyBalls.size() != LotteryTicket.LUCKY_BALL_COUNT){
            throw new InvalidTicketFormatException();
        }
        LotteryTicket ticket = new LotteryTicket(luckyBalls);
        lottery.addTicket(ticket);
        return ticket.getNumber();
    }

    @Override
    public List<String> showTicket(int number) throws FileNotFoundException {
        List<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(new File("ticket/" + number + ".txt"));
        while (scanner.hasNextLine()){
            list.add(scanner.nextLine());
        }
        return list;
    }
}
