package com.RY.Lottery.mvc.view;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import com.RY.Lottery.Exception.InvalidNumberValueOfLuckyBallException;
import com.RY.Lottery.Exception.InvalidTicketFormatException;
import com.RY.Lottery.mvc.controller.Controller;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AddLuckyNumberView {

    private Controller controller;
    private static Scanner input = new Scanner(System.in);
    private boolean isColor;
    private String color;
    private int number;
    private  LotteryView parentView;

    public AddLuckyNumberView(Controller controller , LotteryView parentView) {
        isColor = true;
        this.controller = controller;
        this.parentView = parentView;
    }

    //-------------------------------------------------------------------------

    public void show() {
        String inputValue;
        do {
            if (isColor == true){
                System.out.println("Please, write color RED, YELLOW ,GREEN ,BLUE;");
            }else {
                System.out.println("Please, write number 0 - 99;");
            }

            inputValue = input.nextLine().toUpperCase();

            if (isColor == true){
                color = inputValue.trim();

                isColor = false;
            }else {
                try {
                    number = Integer.parseInt(inputValue.trim());
                } catch (Exception e){
                    //TODO
                    System.out.println("неправлильний формат числа");
                    continue;
                }

                try {
                    controller.addLuckyNumber(color,number);
                    System.out.println("LuckyBall added");
                } catch (InvalidNumberValueOfBallColorException | InvalidNumberValueOfLuckyBallException e) {
                    System.out.println("LuckyBall is not valid ");
                } finally {
                    isColor = true;
                }
            }
            if (inputValue.equals("END")){
                try {
                    System.out.println("ticket number = "+controller.addTicket());
                } catch (FileNotFoundException  | InvalidTicketFormatException e) {
                    System.out.println("Invalid Ticket Format ");
                }
                parentView.show();
                return;
            }
        } while (!inputValue.equals("END"));
    }
}
