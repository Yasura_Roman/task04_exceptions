package com.RY.Lottery.mvc.view;

import com.RY.Lottery.mvc.controller.Controller;
import com.RY.Lottery.mvc.controller.LotteryController;

import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LotteryView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public LotteryView() {
        controller = new LotteryController();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - create ticket");
        menu.put("2", "  2 - show ticket");
        menu.put("3", "  3 - start lottery");
        menu.put("4", "  4 - new Lottery");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1(){
        controller.createNewTicket();
        new AddLuckyNumberView(controller,this).show();
    }

    private void pressButton2() {
        System.out.println("input ticket number");
        List<String> list;
        try {
            list = controller.showTicket(Integer.parseInt(input.nextLine()));
            for (String s:list
            ) {
                System.out.println(s);
            }
        } catch (FileNotFoundException e) {
            System.out.println("ticket not found number");
        }
    }

    private void pressButton3() {
        System.out.println("Winner ticket number ");
        int fdsfa = controller.startLottery();
        System.out.println(fdsfa);
    }

    private void pressButton4() {
        controller.newLottery();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
