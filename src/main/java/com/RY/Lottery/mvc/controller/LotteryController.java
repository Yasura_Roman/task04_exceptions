package com.RY.Lottery.mvc.controller;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import com.RY.Lottery.Exception.InvalidNumberValueOfLuckyBallException;
import com.RY.Lottery.Exception.InvalidTicketFormatException;
import com.RY.Lottery.mvc.model.Model;
import com.RY.Lottery.mvc.model.ModelImp;

import java.io.FileNotFoundException;
import java.util.List;

public class LotteryController implements Controller {

    Model model = new ModelImp();
    @Override
    public int startLottery() {
        return model.startLottery();
    }

    @Override
    public void newLottery() {
        model.newLottery();
    }

    @Override
    public void createNewTicket() {
        model.createNewTicket();
    }

    @Override
    public void addLuckyNumber(String color, int number) throws InvalidNumberValueOfBallColorException, InvalidNumberValueOfLuckyBallException {
        model.addLuckyNumber(color,number);
    }

    @Override
    public int addTicket() throws FileNotFoundException , InvalidTicketFormatException{
        return model.addTicket();
    }

    @Override
    public List<String> showTicket(int number) throws FileNotFoundException {
        return model.showTicket(number);
    }
}
