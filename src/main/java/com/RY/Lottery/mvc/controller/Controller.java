package com.RY.Lottery.mvc.controller;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import com.RY.Lottery.Exception.InvalidNumberValueOfLuckyBallException;
import com.RY.Lottery.Exception.InvalidTicketFormatException;

import java.io.FileNotFoundException;
import java.util.List;

public interface Controller {
    int startLottery();
    void newLottery();
    void createNewTicket();
    void addLuckyNumber(String color , int number) throws InvalidNumberValueOfBallColorException, InvalidNumberValueOfLuckyBallException;
    int addTicket() throws FileNotFoundException, InvalidTicketFormatException;
    List<String> showTicket(int number) throws FileNotFoundException;
}
