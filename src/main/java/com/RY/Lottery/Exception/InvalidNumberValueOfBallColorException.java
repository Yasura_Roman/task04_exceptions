package com.RY.Lottery.Exception;

public class InvalidNumberValueOfBallColorException extends Exception {

    public InvalidNumberValueOfBallColorException() {
        super();
    }

    public InvalidNumberValueOfBallColorException(String message) {
        super(message);
    }

    public InvalidNumberValueOfBallColorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidNumberValueOfBallColorException(Throwable cause) {
        super(cause);
    }
}
