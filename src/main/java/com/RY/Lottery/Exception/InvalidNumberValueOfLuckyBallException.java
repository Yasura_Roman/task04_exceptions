package com.RY.Lottery.Exception;

public class InvalidNumberValueOfLuckyBallException extends Exception {
    public InvalidNumberValueOfLuckyBallException() {
        super();
    }

    public InvalidNumberValueOfLuckyBallException(String message) {
        super(message);
    }

    public InvalidNumberValueOfLuckyBallException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidNumberValueOfLuckyBallException(Throwable cause) {
        super(cause);
    }

    protected InvalidNumberValueOfLuckyBallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
