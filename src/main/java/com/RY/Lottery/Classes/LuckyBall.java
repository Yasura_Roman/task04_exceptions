package com.RY.Lottery.Classes;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import com.RY.Lottery.Exception.InvalidNumberValueOfLuckyBallException;

import java.util.Random;

/**
 *
 * value - is integer number between 0 to 99
 */
public class LuckyBall {

    static final int MIN_VALUE = 0;
    static final int MAX_VALUE = 99;
    private int value;
    private BallColor color;

    public LuckyBall() throws InvalidNumberValueOfBallColorException {
        Random random = new Random();
        value = random.nextInt(MAX_VALUE+1);
        int colorValue =random.nextInt(10) % BallColor.values().length;
        color = BallColor.getInstance(colorValue);
    }

    public LuckyBall(int value, BallColor color) throws InvalidNumberValueOfLuckyBallException {
        if (value>MAX_VALUE || value<MIN_VALUE){
            throw new InvalidNumberValueOfLuckyBallException();
        }
        this.value = value;
        this.color = color;
    }

    @Override
    public String toString() {
        return "LuckyBall [color = " + color.name()+ "; number = " +value+ "]";
    }
}
