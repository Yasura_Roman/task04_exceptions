package com.RY.Lottery.Classes;

import com.RY.Lottery.Exception.InvalidTicketFormatException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class LotteryTicket implements AutoCloseable {

    static int id = 0;
    public static final int LUCKY_BALL_COUNT = 10;

    private int number;
    private List<LuckyBall> luckyBalls;

    private PrintWriter file;

    public LotteryTicket(List<LuckyBall> luckyBalls) throws FileNotFoundException {
        number = id++;
        this.luckyBalls = luckyBalls;

        try {
            file = new PrintWriter("ticket/"+this.number + ".txt");
            //file.println();
            for (LuckyBall lb: luckyBalls
                 ) {
                file.println(lb);
            }
            file.flush();
        } catch (FileNotFoundException e) {
            throw e;
        }
    }

    public void close() throws Exception {
        if (luckyBalls.size() != LUCKY_BALL_COUNT){
            throw new InvalidTicketFormatException();
        }
        if(file != null){
            file.close();
        }
    }

    public int getNumber() {
        return number;
    }

    public List<LuckyBall> getLuckyBalls() {
        return luckyBalls;
    }
}
