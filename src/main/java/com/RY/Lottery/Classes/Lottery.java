package com.RY.Lottery.Classes;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;
import java.io.FileNotFoundException;
import java.util.*;

public class Lottery {

    List<LotteryTicket> tickets;

    public Lottery() {
        tickets = new LinkedList<>();
    }

    public int start(){
        Random random = new Random();
        Map<LotteryTicket,Integer> map= new HashMap<>();
        List<LuckyBall> luckyBalls = new ArrayList<>();

        while (tickets.size() < 50){
            try {
                tickets.add(new LotteryTicket(new ArrayList<LuckyBall>(){
                    {
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                        add(new LuckyBall());
                    }
                }));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InvalidNumberValueOfBallColorException e) {
                e.printStackTrace();
            }
        }

        for (LotteryTicket ticket:tickets
             ) {
            map.put(ticket,0);
        }

        try {
            luckyBalls.add(new LuckyBall());
            luckyBalls.add(new LuckyBall());
            luckyBalls.add(new LuckyBall());
        } catch (InvalidNumberValueOfBallColorException e) {
            e.printStackTrace();
        }

        for (LotteryTicket ticket:map.keySet()) {
            for (int i = 0; i < luckyBalls.size(); i++) {
                if(ticket.getLuckyBalls().contains(luckyBalls.get(i))){
                    map.put(ticket,map.get(ticket)+1);
                }
            }
        }
        Optional<Map.Entry<LotteryTicket, Integer>> winner = map
                .entrySet()
                .stream()
                .max((e1, e2) -> e1.getValue().compareTo(e2.getValue()));
        return winner.get().getKey().getNumber();
    }

    public Lottery(List<LotteryTicket> tickets) {
        this.tickets = tickets;
    }

    public List<LotteryTicket> getTickets() {
        return tickets;
    }

    public void setTickets(List<LotteryTicket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(LotteryTicket ticket) {
        tickets.add(ticket);
    }
}
