package com.RY.Lottery.Classes;

import com.RY.Lottery.Exception.InvalidNumberValueOfBallColorException;

public enum BallColor {
    RED(0),
    YELLOW(1),
    GREEN(2),
    BLUE(3);

    int value;
    BallColor(int v){
        value =v;
    }

    public int getValue() {
        return value;
    }

    public static BallColor getInstance(int value) throws InvalidNumberValueOfBallColorException {
        for (BallColor color: values()
             ) {
            if(color.getValue() == value){
                return color;
            }
        }
        throw new InvalidNumberValueOfBallColorException();
    }

    public static BallColor getInstance(String value) throws InvalidNumberValueOfBallColorException {
        for (BallColor color: values()
        ) {
            if(color.name().equals(value.toUpperCase())){
                return color;
            }
        }
        throw new InvalidNumberValueOfBallColorException();
    }
}
